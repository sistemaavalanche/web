export default {
  state: {
    // Dados de sessão e acesso
    token: String(),
    type: String(), // {'user', 'admin'}
    profiles: [], // Todos os perfis que ele tem acesso, ex ['athlete', 'student', 'finance']
    sportGroups: [], // Todos os esportes e grupos (cheers, bateria...) que participa

    // Dados básicos do usuario
    fullname: String(),
    nickname: String(),
    birthdate: Date(),
    cpf: Number(),
    gender: String(), // (1){'M','F','O'}
    document: {
      number: String(),
      issuer: String(),
      UF: String() // (2)[Estados do Brasil]
    },

    // Dados de estudo
    uni: {
      name: String(),
      abbreviation: String(),
      city: String(),
      UF: String(),
      country: String(),
      RA: String(),
      course: String(),
    },

    // Dados de contato
    email: String(),
    phone: Number(),
    emergencyContact: {
      phone: Number(),
      name: String(),
      type: String() // Grau de parentesco
    },

    // Dados de Saúde
    bloodType: String(), // (3) Tipo + Sinal [O+, B-, AB+, ...] ou 'IDK'
    insurance: {
      name: String(),
      reach: [] // Estados e países que abrange o plano
    },
    medications: [],
    diseases: [],
  },
  getters: {
    name(state) {
      return state.fullname.replace(/ .*/, '')
    },
    lastname(state) {
      return state.fullname.replace(/.* /, '')
    },
    isUTFPR(state) {
      return state.uni.abbreviation === 'UTFPR'
    }
  },
  mutations: {
    setLoginData(state, {
      cpf,
      token,
      type,
      profiles,
    }) {
      state = {
        ...state,
        cpf,
        token,
        type,
        profiles
      }
    }
  },
  actions: {}

}